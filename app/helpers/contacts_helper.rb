module ContactsHelper
  def form_submit_path
    params[:action] == "edit" || params[:action] == "update"  ? update_contact_path(@contact._id) : create_contact_path
  end
end
