class Contact
  include Her::Model

  attributes :name, :numbers
  
  validates :name, presence: true
  validates :numbers, presence: true

  custom_post :search
end