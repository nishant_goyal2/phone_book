class ContactsController < ApplicationController
  before_filter :set_contact, :only => ["show", "edit", "update"]
  before_filter :sanitzie_numbers_params, :only => ["create", "update"]
  def index
    @contacts = Contact.all
  end
  
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    if @contact.valid? && @contact.save
      redirect_to contacts_path, :notice => "Contact created successfully."
    else
      @errors = @contact.errors.full_messages
    end
  end


  def show
  end

  def edit
  end

  def update
    @contact.assign_attributes(params[:contact])
    if @contact.valid? && Contact.save_existing(params[:id], params[:contact])
      redirect_to contacts_path, :notice => "Contact updated successfully."
    else
      @errors = @contact.errors.full_messages
    end
  end

  def destroy
    Contact.destroy_existing(params[:id])
    redirect_to contacts_path, :notice => "Contact deleted successfully."
  end

  def search
    @contacts = Contact.search(params[:contact][:search_attribute].to_sym => params[:contact][:search_query])
    render :index
  end

  private
  def set_contact
    @contact = Contact.find(params[:id])
  end

  def sanitzie_numbers_params
    params[:contact][:numbers] = params[:contact][:numbers].values
  end
end
